const express = require('express')
const router = express.Router()
const User = require('../models/User')

router.get('/', (req, res) => {
    res.send('hello user')
})

router.post('/signup', async (req, res) => {
    const user = new User({
        name : req.body.name,
        password : req.body.password,
    })
    try {
        const userCreated = await user.save()
        res.send(userCreated)        
    } catch (error) {
        res.status(500).send({error: 'error creation'});
    }
})


router.post('/login', async (req, res) => {
    const user = await User.findOne({name: req.body.name})

    const isMatch = false;
    try {  
        if(user.password !== req.body.password) return res.send({success: false,message:'password incorrect'})

        res.send({success: true, data: user})
    } catch (error) {
        res.status(500).send({error: 'check your name & password'});
    }
})

router.patch('/', async (req, res) => {
    try {
        const userUpdated = await User.findByIdAndUpdate(req.body.id,{
            name: req.body.name,
            password: req.body.password,
            img: req.body.img
        }
            )
        
    } catch (error) {
        res.status(500).send({error: 'fail update'})
    }
})

router.delete('/:userId', async (req, res) => {
    try {
        const userRemoved = await User.A.findByIdAndRemove(req.params.userId)
        res.status(200).send('user deleted')
    } catch (error) {
        res.status(500).send({error: 'fail delete'})
    }
})


module.exports = router